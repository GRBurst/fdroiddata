{ pkgs ? import <nixpkgs> {} }:

# TODO: Check https://nixos.org/nixpkgs/manual/#building-an-android-application
let fhs = pkgs.buildFHSUserEnv {
  name = "android-env";
  targetPkgs = pkgs: with pkgs;
    [ git
      gitRepo
      gnupg
      python2
      curl
      procps
      openssl
      gnumake
      nettools
      androidenv.androidPkgs_9_0.androidsdk
      jdk
      schedtool
      utillinux
      m4
      gperf
      perl
      libxml2
      zip
      unzip
      bison
      flex
      lzop
    ];
  multiPkgs = pkgs: with pkgs;
    [ zlib
    ];
  profile = ''
    export USE_CCACHE=1
    export ANDROID_JAVA_HOME=${pkgs.jdk.home}
    export ANDROID_HOME=${pkgs.androidenv.androidPkgs_9_0.androidsdk}/libexec/android-sdk/
  '';
};

flutterPkgs = (import (builtins.fetchTarball "https://github.com/babariviere/nixpkgs/archive/flutter-init.tar.gz")  {});
dartPkgs    = (import (builtins.fetchTarball "https://github.com/GRBurst/nixpkgs/archive/dart.tar.gz")              {});

in

pkgs.stdenv.mkDerivation {
  name = "fdroidPythonEnv";

  src = null;

  nativeBuildInputs = [ fhs ];
  buildInputs = with pkgs; [
  # buildInputs = [
    # these packages are required for virtualenv and pip to work:
    python37Packages.docker
    python37Packages.virtualenv
    python37Packages.pip
    python37Packages.pylint
    python37Packages.setuptools
    python37Packages.six
    # the following packages are related to the dependencies of your python
    # project.
    android-studio
    git
    stdenv
    flutterPkgs.flutter
    dartPkgs.dart
    python37Packages.androguard
    python37Packages.clint
    python37Packages.defusedxml
    python37Packages.docker
    python37Packages.docker-py
    python37Packages.GitPython
    python37Packages.libcloud
    python37Packages.mwclient
    python37Packages.paramiko
    python37Packages.pillow
    python37Packages.pyasn1
    python37Packages.pyasn1-modules
    python37Packages.python-vagrant
    python37Packages.pyyaml
    python37Packages.qrcode
    python37Packages.requests
    python37Packages.ruamel_yaml
  ];

  shellHook = ''
    # set SOURCE_DATE_EPOCH so that we can use python wheels
    SOURCE_DATE_EPOCH=$(date +%s)
    virtualenv --no-setuptools venv
    export PATH=$PWD/venv/bin:$PATH
    export PATH=~/projects/fdroidserver:$PATH
    exec android-env
  '';
}
